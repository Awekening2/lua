---------------------------------------------------------------------------------
--
-- scene.lua
-- autor: Alexandr
-- date: 11.11.2016
---------------------------------------------------------------------------------

local title =...

local composer = require( "composer" )
local widget = require("widget")
local json = require ("json")
local http = require ("socket.http")


local scene = composer.newScene(title)


function scene:create( event )
    
    local sceneGroup = self.view
  
        -- таблица с именами файлов 
    name ={}

    local LogFileName = system.pathForFile("logfile.log",system.DocumentsDirectory)
    --открываем лог файл
    local logfile = io.open(LogFileName,"r")
    -- если лог файла нет, то совершается первое подключение к сети и создание лог файла  
    if not logfile then
        
    --задаем путь до .json файла 
    local filename = system.pathForFile("images.json", system.ResourceDirectory)

    -- пробуем декодировать файл 
    local decoded = json.decodeFile(filename)
    
    if not decoded then 
        print ("Eror file decoded")
        return 
        else
            
       
        --разберем таблицу полученную из .json
        for _, val in pairs(decoded) do 
            for key,value in pairs(val) do
                
                -- получаем файл по URL 
                local body, code = http.request(value)
                    
                    if not body then 
                       print (code)
                        return
                     else   
                        print(value)

                        -- получаем имя файла и создадим сам файл под этим именем 
                        name[key] = value:match("([^/]+)$")
                        -- файл
                        local path = system.pathForFile(name[key],system.DocumentsDirectory)
                        logfile = io.open(LogFileName,"a")
                        local file = io.open(path,"wb")
                        -- сохраним картинку
                        file:write(body)
                        -- запишем в лог 
                        logfile:write(name[key],"\n")

                        io.close(file)
                        io.close(logfile)
                    end
            end
        end
                
    end
    -- если лог файл уже был создам ранее, то используем его 
        else 
        -- заполняем таблицу с именами файлов
            for line in io.lines(LogFileName) do
            table.insert(name,line)
            end
            
        
        
    end
end


function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    -- отступ от начала скролла
    local padding=display.contentHeight/2
    
    if ( phase == "will" ) then
    ---------------------------------------------------------------------------------
    -- скролл
     scrollView = widget.newScrollView(
        {
            left=0,
            top=0,
            width = ScreenW,
            height =ScreenH,
            listener = scrollListener,
            verticalScrollDisabled = false,
            horizontalScrollDisabled =true,
            backgroundColor={0.5,0.5,0.5},
            topPadding = 0,
        bottomPadding = 0,
        leftPadding = 0,
        rightPadding = 0,
        }
    )
    ---------------------------------------------------------------------------------
    elseif ( phase == "did" ) then
        
        -- разберем таблицу с именами файлов
        for _,value in pairs(name) do
            local Image,errorCode = display.newImageRect(value,system.DocumentsDirectory,display.contentWidth-10,display.contentHeight)
            -- если image получен, то добавим его в скролл
            if Image then  
                
                Image.x=display.contentCenterX
                Image.y=padding

                scrollView:insert(Image)

                padding =padding+display.contentHeight+20
            else 
            print(errorCode)
            end
        end
    end
end
 
----------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
---------------------------------------------------------------------------------


return scene
 

